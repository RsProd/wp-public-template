<!doctype html>
<html lang="ru">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php

echo get_bloginfo('name');
echo get_bloginfo('description');

wp_nav_menu(array(
	'theme_location' => 'primary',
	'container' => 'ul',
	'menu_class' => 'menu'
));

?>
