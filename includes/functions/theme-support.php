<?php

add_action('after_setup_theme', function () {
	load_theme_textdomain(TEXT_DOMAIN, get_template_directory() . '/languages');
});

if (function_exists('add_theme_support')) {
	add_theme_support('title-tag');
	add_theme_support('menus');
	add_theme_support('post-thumbnails', array('page', 'post'));
}

register_nav_menus(array(
	'primary' => 'Основное меню',
	'second' => 'Дополнительное меню'
));

//if (function_exists('add_image_size')) {
//	add_image_size('slider', 800, 460, true);
//}

add_filter('mime_types', 'webp_upload_mimes');
function webp_upload_mimes($existing_mimes) {
	$existing_mimes['webp'] = 'image/webp';
	return $existing_mimes;
}
