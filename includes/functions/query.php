<?php

function getPosts($args, $format_date = 'Y.m.d') {
	$isAjax = wp_doing_ajax();
	$data = $isAjax ? $_POST : $args;

	$defaults = [
		"post_type" => "post",
		"order" => "DESC",
		"posts_per_page" => get_option('post_per_page'),
	];

	$parsed_data = wp_parse_args($data, $defaults);

	$q = new WP_Query();
	$posts = $q->query($parsed_data);

	$result = [
		'posts' => [],
		'total' => $q->found_posts,
		'debug' => $parsed_data
	];

	foreach ($posts as $post) {

		$result['posts'][] = [
			"id" => $post->ID,
			"title" => $post->post_title,
			"description" => $post->post_content ? wp_kses(do_limit($post->post_content, 10), []) : '',
			"date" => get_the_date($format_date, $post->ID),
			"thumbnail" => has_post_thumbnail($post->ID) ? get_thumbnails_all_format(get_the_post_thumbnail_url($post->ID, 'large')) : "not image",
			"link" => get_permalink($post->ID)
		];
	}

	return $isAjax ? die(json_encode($result)) : $result;

}

add_action('wp_ajax_get_posts', 'get_posts');
add_action('wp_ajax_nopriv_get_posts', 'get_posts');
