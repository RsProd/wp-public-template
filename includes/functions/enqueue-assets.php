<?php

//Подключение стилей
function enqueue_styles() {
	wp_dequeue_style('wp-block-library');
	wp_enqueue_style('font-style');

	wp_enqueue_style('main', PRODUCTION ?
		assets("styles/main.min.css") :
		assets("styles/main.css"), false, PRODUCTION ? 7.4 : time());
}

add_action('wp_enqueue_scripts', 'enqueue_styles');

//Подключение скриптов
function enqueue_script() {
	wp_deregister_script('jquery');
	wp_deregister_script('wp-embed');

	wp_enqueue_script('vendor', PRODUCTION ?
		assets('scripts/vendor.min.js') :
		assets('scripts/vendor.js'),false, PRODUCTION ? 7.4 : time(), true);

	wp_enqueue_script('main', PRODUCTION ?
		assets("scripts/main.min.js") :
		assets("scripts/main.js"), array('vendor'), PRODUCTION ? 7.4 : time(), true);
}

add_action('wp_enqueue_scripts', 'enqueue_script', 11);
