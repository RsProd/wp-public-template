<?php

function enqueue_localize_script() {
	wp_localize_script('vendor', 'path', array(
		'ajax' => admin_url('admin-ajax.php'),
		'theme' => get_stylesheet_directory_uri()
	));
}

add_action('wp_enqueue_scripts', 'enqueue_localize_script', 12);
