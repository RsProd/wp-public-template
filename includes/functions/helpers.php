<?php

//Debug
function debug($obj) {
	ob_start(); ?>
	<pre>
			<?php print_r($obj) ?>
		</pre>
	<?php return ob_get_clean();
}

function assets($file) {
	return TH_PATH . '/assets/' . $file;
}

//Ограничение отрывка по словам
function do_limit($string, $word_limit) {
	$words = explode(' ', $string, ($word_limit + 1));
	if (count($words) > $word_limit)
		array_pop($words);
	return implode(' ', $words) . ' ...';
}

//Получение иконок из svg спрайта
function svg($icon, $class) {
	return '<svg class="' . $class . ' ' . $class . '_' . $icon . '">
		<use xlink:href="' . assets('icons/sprite.svg?v4.8#') . $icon . '"></use>
	</svg>';
}
