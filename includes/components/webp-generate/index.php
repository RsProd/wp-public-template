<?php

// ВАЖНО!!! Для работы необходимо, что бы на сервере был установлен модуль imagick.

function gt_webp_generation($metadata) {
	$uploads = wp_upload_dir(); // получает папку для загрузки медиафайлов

	$file = $uploads['basedir'] . '/' . $metadata['file']; // получает исходный файл
	$fileName = pathinfo($file)['filename']; // Получает имя файла
	$ext = wp_check_filetype($file); // получает расширение файла

	if ($ext['type'] == 'image/jpeg') { // в зависимости от расширения обрабатаывает файлы разными функциями
		$image = imagecreatefromjpeg($file); // создает изображение из jpg

	} elseif ($ext['type'] == 'image/png') {
		$image = imagecreatefrompng($file); // создает изображение из png
		imagepalettetotruecolor($image); // восстанавливает цвета
		imagealphablending($image, false); // выключает режим сопряжения цветов
		imagesavealpha($image, true); // сохраняет прозрачность
	}

	imagewebp($image, $uploads['path'] . '/' . $fileName . '.webp', 70);

	foreach ($metadata['sizes'] as $size) { // перебирает все размеры файла и также сохраняет в webp
		$file = $uploads['url'] . '/' . $size['file'];
		$fileName = pathinfo($size['file'])['filename'];

		$ext = $size['mime-type'];

		if ($ext == 'image/jpeg') {
			$image = imagecreatefromjpeg($file);

		} elseif ($ext == 'image/png') {
			$image = imagecreatefrompng($file);
			imagepalettetotruecolor($image);
			imagealphablending($image, false);
			imagesavealpha($image, true);
		}

		imagewebp($image, $uploads['path'] . '/' . $fileName . '.webp', 70);


	}

	return $metadata;
}

if (WEBP_SUPPORT) add_filter('wp_generate_attachment_metadata', 'gt_webp_generation');


function get_thumbnails_all_format($img_src) {

	$src = str_replace(get_site_url() . '/', '', $img_src);
	$src_format = !empty($src) ? pathinfo($src)['extension'] : false;
	$absolute_path = !empty($src) ? ABSPATH . $src : false;

	$webp = [
		"absolute" => ($src_format) ? pathinfo($absolute_path)["dirname"] . '/' . pathinfo($absolute_path)["filename"] . '.webp' : false,
		"relative" => ($src_format) ? pathinfo($src)["dirname"] . '/' . pathinfo($src)["filename"] . '.webp' : false,
	];

	if (WEBP_SUPPORT) {
		return [
			"webp" => [
				"src" => file_exists($webp["absolute"]) ? '/' . $webp['relative'] : '',
				"type" => "image/webp"
			],
			"img" => [
				"src" => '/' . $src,
				"type" => "image/$src_format"
			]
		];
	} else {
		return $img_src;
	}
}
