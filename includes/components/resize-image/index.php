<?php

$PLUGIN_VERSION = '1.7.2';
$DEBUG_LOGGER = false;


// Default plugin values
if(get_option('jr_resizeupload_version') != $PLUGIN_VERSION) {

  add_option('jr_resizeupload_version', 			$PLUGIN_VERSION, '','yes');
  add_option('jr_resizeupload_width', 				'1200', '', 'yes');
  add_option('jr_resizeupload_height',				'1200', '', 'yes');
  add_option('jr_resizeupload_quality',				'90', '', 'yes');
  add_option('jr_resizeupload_resize_yesno', 		'yes', '','yes');
  add_option('jr_resizeupload_recompress_yesno', 	'no', '','yes');
  add_option('jr_resizeupload_convertbmp_yesno', 	'no', '', 'yes');
  add_option('jr_resizeupload_convertpng_yesno', 	'no', '', 'yes');
  add_option('jr_resizeupload_convertgif_yesno', 	'no', '', 'yes');
}



// Hook in the options page
add_action('admin_menu', 'jr_uploadresize_options_page');

// Hook the function to the upload handler
add_action('wp_handle_upload', 'jr_uploadresize_resize');




/**
* Add the options page
*/
function jr_uploadresize_options_page(){
	if(function_exists('add_options_page')){
		add_options_page(
			'Изменить размер изображения после загрузки',
			'Оптимизация изоб.',
			'manage_options',
			'resize-after-upload',
			'jr_uploadresize_options'
		);
	}
} // function jr_uploadresize_options_page(){



/**
* Define the Options page for the plugin
*/
function jr_uploadresize_options(){

  if(isset($_POST['jr_options_update'])) {

    $resizing_enabled = trim(esc_sql($_POST['yesno']));
    $force_jpeg_recompression   = trim(esc_sql($_POST['recompress_yesno']));

    $max_width   = trim(esc_sql($_POST['maxwidth']));
    $max_height  = trim(esc_sql($_POST['maxheight']));
    $compression_level    = trim(esc_sql($_POST['quality']));

    $convert_png_to_jpg = trim(esc_sql(isset($_POST['convertpng']) ? $_POST['convertpng'] : 'no'));
    $convert_gif_to_jpg = trim(esc_sql(isset($_POST['convertgif']) ? $_POST['convertgif'] : 'no'));
    $convert_bmp_to_jpg = trim(esc_sql(isset($_POST['convertbmp']) ? $_POST['convertbmp'] : 'no'));


    // If input is not an integer, use previous setting
    $max_width = ($max_width == '') ? 0 : $max_width;
    $max_width = (ctype_digit(strval($max_width)) == false) ? get_option('jr_resizeupload_width') : $max_width;
    update_option('jr_resizeupload_width',$max_width);


    $max_height = ($max_height == '') ? 0 : $max_height;
    $max_height = (ctype_digit(strval($max_height)) == false) ? get_option('jr_resizeupload_height') : $max_height;
    update_option('jr_resizeupload_height',$max_height);


    $compression_level = ($compression_level == '') ? 1 : $compression_level;
    $compression_level = (ctype_digit(strval($compression_level)) == false) ? get_option('jr_resizeupload_quality') : $compression_level;

    if($compression_level < 1) {
    	$compression_level = 1;
    }
    else if($compression_level > 100) {
    	$compression_level = 100;
    }

    update_option('jr_resizeupload_quality',$compression_level);




    if ($resizing_enabled == 'yes') {
      update_option('jr_resizeupload_resize_yesno','yes'); }
    else {
      update_option('jr_resizeupload_resize_yesno','no'); }


    if ($force_jpeg_recompression == 'yes') {
      update_option('jr_resizeupload_recompress_yesno','yes'); }
    else {
      update_option('jr_resizeupload_recompress_yesno','no'); }


    if ($convert_png_to_jpg == 'yes') {
      update_option('jr_resizeupload_convertpng_yesno','yes'); }
    else {
      update_option('jr_resizeupload_convertpng_yesno','no'); }

    if ($convert_gif_to_jpg == 'yes') {
      update_option('jr_resizeupload_convertgif_yesno','yes'); }
    else {
      update_option('jr_resizeupload_convertgif_yesno','no'); }

    if ($convert_bmp_to_jpg == 'yes') {
      update_option('jr_resizeupload_convertbmp_yesno','yes'); }
    else {
      update_option('jr_resizeupload_convertbmp_yesno','no'); }



    echo('<div id="message" class="updated fade"><p><strong>Options have been updated.</strong></p></div>');
  } // if



  // get options and show settings form
  $resizing_enabled = get_option('jr_resizeupload_resize_yesno');
  $force_jpeg_recompression = get_option('jr_resizeupload_recompress_yesno');
  $compression_level  = intval(get_option('jr_resizeupload_quality'));

  $max_width     = get_option('jr_resizeupload_width');
  $max_height    = get_option('jr_resizeupload_height');

  $convert_png_to_jpg = get_option('jr_resizeupload_convertpng_yesno');
  $convert_gif_to_jpg = get_option('jr_resizeupload_convertgif_yesno');
  $convert_bmp_to_jpg = get_option('jr_resizeupload_convertbmp_yesno');
?>
<style type="text/css">
.resizeimage-button {
  color: #FFF;
  background: none repeat scroll 0% 0% #FC9A24;
  border-radius: 3px;
  display: inline-block;
  border-bottom: 4px solid #EC8A14;
  margin-right:5px;
  line-height:1.05em;
  text-align: center;
  text-decoration: none;
  padding: 9px 20px 8px;
  font-size: 15px;
  font-weight: bold;
  text-shadow: 0 -1px 1px rgba(0,0,0,0.2);
}

.resizeimage-button:active,
.resizeimage-button:hover,
.resizeimage-button:focus {
  background-color: #EC8A14;
  color: #FFF;
}

.media-upload-form div.error, .wrap div.error, .wrap div.updated {
  margin: 25px 0px 25px;
}

</style>

<div class="wrap">
	<form method="post" accept-charset="utf-8">

		<h2>Изменение размера изображения после загрузки</h2>

		<h3>Изменение размеров</h3>
		<table class="form-table">
			<tr>
				<th scope="row">Включить изменение размеров</th>
				<td valign="top">
					<select name="yesno" id="yesno">
						<option value="no" label="Нет" <?php echo ($resizing_enabled == 'no') ? 'selected="selected"' : ''; ?>>Нет - Не изменять размер изображений</option>
						<option value="yes" label="Да" <?php echo ($resizing_enabled == 'yes') ? 'selected="selected"' : ''; ?>>Да - Изменить размер больших изображений</option>
					</select>
				</td>
			</tr>

			<tr>
				<th scope="row">Макс. Размер изображения</th>

				<td>
					<fieldset><legend class="screen-reader-text"><span>Максимальная ширина и высота</span></legend>
						<label for="maxwidth">Макс. ширина</label>
						<input name="maxwidth" step="1" min="0" id="maxwidth" class="small-text" type="number" value="<?php echo $max_width; ?>">
						&nbsp;&nbsp;&nbsp;<label for="maxheight">Макс. высота</label>
						<input name="maxheight" step="1" min="0" id="maxheight" class="small-text" type="number" value="<?php echo $max_height; ?>">
						<p class="description">Установите значение желаемого размера изображений.
						<br />Рекомендуемые значения: <code>1200</code></p>
					</fieldset>
				</td>


			</tr>

		</table>

		<hr style="margin-top:20px; margin-bottom:30px;">

		<h3>Параметры сжатия</h3>
		<p style="max-width:700px">Следующие настройки применяются только к загруженным изображениям JPEG и изображениям, преобразованным в формат JPEG.</p>

		<table class="form-table">

			<tr>
				<th scope="row">Уровень сжатия JPEG</th>
				<td valign="top">
					<select id="quality" name="quality">
					<?php for($i=1; $i<=100; $i++) : ?>
						<option value="<?php echo $i; ?>" <?php if($compression_level == $i) : ?>selected<?php endif; ?>><?php echo $i; ?></option>
					<?php endfor; ?>
					</select>
					<p class="description"><code>1</code> = Низкое качество (самые маленькие файлы)
					<br><code>100</code> = Лучшее качество (самые большие файлы)
					<br>Рекомендуемые значения: <code>90</code></p>
				</td>
			</tr>

			<tr>
				<th scope="row">Повторное сжатие принудительного JPEG</th>
				<td>
					<select name="recompress_yesno" id="yesno">
						<option value="no" label="Нет" <?php echo ($force_jpeg_recompression == 'no') ? 'selected="selected"' : ''; ?>>NO - only re-compress resized jpeg images</option>
						<option value="yes" label="Да" <?php echo ($force_jpeg_recompression == 'yes') ? 'selected="selected"' : ''; ?>>YES - re-compress all uploaded jpeg images</option>
					</select>
				</td>
			</tr>

		</table>

		<hr style="margin-top:30px;">

		<p class="submit" style="margin-top:10px;border-top:1px solid #eee;padding-top:20px;">
		  <input type="hidden" id="convert-bmp" name="convertbmp" value="no" />
		  <input type="hidden" name="action" value="update" />
		  <input id="submit" name="jr_options_update" class="button button-primary" type="submit" value="Update Options">
		</p>
	</form>

</div>
<?php
} // function jr_uploadresize_options(){





/**
* This function will apply changes to the uploaded file
* @param $image_data - contains file, url, type
*/
function jr_uploadresize_resize($image_data){


  jr_error_log("**-start--resize-image-upload");


  $resizing_enabled = get_option('jr_resizeupload_resize_yesno');
	$resizing_enabled = ($resizing_enabled=='yes') ? true : false;

  $force_jpeg_recompression = get_option('jr_resizeupload_recompress_yesno');
	$force_jpeg_recompression = ($force_jpeg_recompression=='yes') ? true : false;

  $compression_level = get_option('jr_resizeupload_quality');


  $max_width  = get_option('jr_resizeupload_width')==0 ? false : get_option('jr_resizeupload_width');

  $max_height = get_option('jr_resizeupload_height')==0 ? false : get_option('jr_resizeupload_height');


  $convert_png_to_jpg = get_option('jr_resizeupload_convertpng_yesno');
	$convert_png_to_jpg = ($convert_png_to_jpg=='yes') ? true : false;

  $convert_gif_to_jpg = get_option('jr_resizeupload_convertgif_yesno');
	$convert_gif_to_jpg = ($convert_gif_to_jpg=='yes') ? true : false;

  $convert_bmp_to_jpg = get_option('jr_resizeupload_convertbmp_yesno');
	$convert_bmp_to_jpg = ($convert_bmp_to_jpg=='yes') ? true : false;



  //---------- In with the old v1.6.2, new v1.7 (WP_Image_Editor) ------------

  if($resizing_enabled || $force_jpeg_recompression) {

		$fatal_error_reported = false;
		$valid_types = array('image/gif','image/png','image/jpeg','image/jpg');

    if(empty($image_data['file']) || empty($image_data['type'])) {
    	jr_error_log("--non-data-in-file-( ".print_r($image_data, true)." )");
		  $fatal_error_reported = true;
    }
    else if(!in_array($image_data['type'], $valid_types)) {
    	jr_error_log("--non-image-type-uploaded-( ".$image_data['type']." )");
		  $fatal_error_reported = true;
    }

    jr_error_log("--filename-( ".$image_data['file']." )");
    $image_editor = wp_get_image_editor($image_data['file']);
    $image_type = $image_data['type'];


    if($fatal_error_reported || is_wp_error($image_editor)) {
      jr_error_log("--wp-error-reported");
    }
    else {

      $to_save = false;
      $resized = false;


      // Perform resizing if required
      if($resizing_enabled) {

        jr_error_log("--resizing-enabled");
        $sizes = $image_editor->get_size();

        if((isset($sizes['width']) && $sizes['width'] > $max_width)
          || (isset($sizes['height']) && $sizes['height'] > $max_height)) {

          $image_editor->resize($max_width, $max_height, false);
          $resized = true;
          $to_save = true;

          $sizes = $image_editor->get_size();
          jr_error_log("--new-size--".$sizes['width']."x".$sizes['height']);
        }
        else {
          jr_error_log("--no-resizing-needed");
        }
      }
      else {
        jr_error_log("--no-resizing-requested");
      }


      // Regardless of resizing, image must be saved if recompressing
      if($force_jpeg_recompression && ($image_type=='image/jpg' || $image_type=='image/jpeg')) {

        $to_save = true;
        jr_error_log("--compression-level--q-".$compression_level);
      }
      elseif(!$resized) {
        jr_error_log("--no-forced-recompression");
      }


      // Only save image if it has been resized or need recompressing
      if($to_save) {

        $image_editor->set_quality($compression_level);
        $saved_image = $image_editor->save($image_data['file']);
        jr_error_log("--image-saved");
      }
      else {
        jr_error_log("--no-changes-to-save");
      }
    }
  } // if($resizing_enabled || $force_jpeg_recompression)

  else {
    jr_error_log("--no-action-required");
  }

  jr_error_log("**-end--resize-image-upload\n");


  return $image_data;
} // function jr_uploadresize_resize($image_data){


/**
* Simple debug logging function. Will only output to the log file
* if 'debugging' is turned on.
*/
function jr_error_log($message) {
  global $DEBUG_LOGGER;

  if($DEBUG_LOGGER) {
    error_log(print_r($message, true));
  }
}
